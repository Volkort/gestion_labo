"""Functions module for gestion_lab"""

import csv
import json
import collections

# CLASSES
class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    """Exception levée si la personne ou le bureau
    cherché n'existe pas dans le labo"""

class PresentException(LaboException):
    """Exception levée si la personne ou le bureau
    cherché existe déjà dans le labo"""


# FUNCTIONS
def creation_labo():
    """Création d'un laboratoire"""
    try : 
        with open("occup_bureau.json", "r") as fichier :
            lab = json.load(fichier)
            if lab == None :
                return {}
        return lab
    except FileNotFoundError :
        return {}


def taille(labo):
    """Définit la taille du labo en nombre de personnes"""
    return len(labo)


def enregistrer_arrivee(labo, nom, bureau):
    """Enregistre une personne et son bureau dans le labo"""
    if nom in labo:
        raise PresentException

    labo[nom] = bureau


def enregistrer_depart(labo, nom):
    """Efface la personne du labo"""
    if nom not in labo:
        raise AbsentException

    del labo[nom]


def modifier_bureau(labo, nom, nouveau_bureau):
    """Change le bureau attribué à une personne"""
    if nom not in labo:
        raise AbsentException

    labo[nom] = nouveau_bureau


def modifier_nom(labo, nom, nouveau_nom):
    """Modifie le nom d'une personne enregistrée dans le labo"""
    if nom not in labo:
        raise AbsentException
    if nouveau_nom in labo:
        raise PresentException
    labo[nouveau_nom] = labo[nom]
    del labo[nom]


def chercher_membre(labo, nom):
    """Permet de vérifier l'existence d'une personne dans un labo"""
    if nom not in labo:
        raise AbsentException

    return labo[nom]


def trouver_bureau(labo, nom):
    """Donne le bureau associé à un membre du laboratoire"""
    if nom not in labo:
        raise AbsentException

    return labo[nom]

def creation_new_labo_sorted(labo):
    """ creer un dictionnaire avec cle et valeur ordonnees"""
    new_labo = {}
    for personne, bureau in labo.items() :
        new_labo.setdefault(bureau, []).append(personne)
    new_labo_sorted = collections.OrderedDict()
    for bureau in sorted(new_labo.keys()) :
        new_labo_sorted[bureau] = sorted(new_labo[bureau])
    return new_labo_sorted

def print_txt(labo):
    """ affichage labo par bureau """
    new_labo_sorted = creation_new_labo_sorted(labo)
    for bureau in new_labo_sorted.keys() :
        print(f"{bureau} :")
        for personne in new_labo_sorted[bureau] :
            print(f"- {personne}")

def print_html(labo):
    """Imprime la liste complète des membres du labo"""
    new_labo_sorted = creation_new_labo_sorted(labo)
    with open("occup_bureau.html", 'w') as fichier:
        html = """<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Occupation des bureaux</title>
    </head>
    <body>\n"""
        for bureau in new_labo_sorted.keys() :
            html += f"<h3>{bureau} :</h3><ul>\n"
            for personne in new_labo_sorted[bureau] :
                html += f"<li>{personne}</li>\n"
        html += "</ul>\n"
        html += "</body></html>"
        fichier.write(html)
    
def import_csv(fichier, labo):
    """ importe un fichier csv """
    with open(fichier, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if labo.get(row['Nom'], False) :
                if labo.get(row['Nom']) != row['Bureau'] :
                    print(f"{row['Nom']} n'est pas dans le bureau {row['Bureau']}")
            else :
                labo[row['Nom']] = row["Bureau"]

def sauvegarder_lab(labo):
    with open("occup_bureau.json", 'w') as fichier:
        json.dump(labo, fichier)
