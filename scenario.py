#!/bin/python3
"""Programme de test du programme de gestion de labos"""

from labo import *


def main():
    lab = creation_labo()
    assert taille(lab) == 0

    enregistrer_arrivee(lab, "Didier", "A204")
    enregistrer_arrivee(lab, "Valérie", "B312")
    enregistrer_arrivee(lab, "Robert", "C317")
    assert lab["Didier"] == "A204"
    assert lab["Valérie"] == "B312"
    assert lab["Robert"] == "C317"
    assert taille(lab) == 3

    enregistrer_depart(lab, "Didier")
    assert taille(lab) == 2
    assert lab["Valérie"] == "B312"
    assert lab["Robert"] == "C317"

    modifier_nom(lab, 'Valérie', "Valoche")
    assert lab["Valoche"] == "B312"
    assert taille(lab) == 2
    

    modifier_bureau(lab, "Valoche", "D306")
    assert lab["Valoche"] == "D306"

    assert chercher_membre(lab, "Robert") == lab["Robert"]


main()
