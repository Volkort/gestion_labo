#!/usr/bin/env python
"""Programme de gestion de labos"""

from labo import *
from menu import *


def gestion_labo():
    """MAIN"""
    lab = creation_labo()
    menu = creation_menu()

    def quitter_labo():
        sauvegarder_lab(lab)
        exit()

    ajouter_entree_menu(menu, "Sauvegarder et Quitter", quitter_labo)

    def commande_arrivee():
        nom = input("Entrez le nom de la personne à enregistrer: ")
        bureau = input("Entrez le bureau du nouvel arrivant: ")
        try :
            enregistrer_arrivee(lab, nom, bureau)
        except PresentException :
            print(f"{nom} est déjà dans le laboratoire.")

    ajouter_entree_menu(menu, "Enregistrer une arrivée", commande_arrivee)

    def commande_depart():
        nom = input("Entrez le nom de la personne à supprimer: ")
        try :
            enregistrer_depart(lab, nom)
        except AbsentException :
            print(f"{nom} est absent du Laboratoire.")

    ajouter_entree_menu(menu, "Enregistrer un départ", commande_depart)

    def commande_modifier_bureau():
        nom = input("Entrez le nom de la personne nécessitant un changement de bureau: ")
        nouveau_bureau = input("Entrez le numéro du nouveau bureau: ")
        try :
            modifier_bureau(lab, nom, nouveau_bureau)
        except AbsentException :
            print(f"{nom} est absent du Laboratoire.")

    ajouter_entree_menu(menu, "Modifier le bureau d'un membre du personnel", commande_modifier_bureau)

    def commande_modifier_nom(labo):
        nom = input("Entrez le nom de la personne nécessitant un changement de nom: ")
        nouveau_nom = input("Entrez le nouveau nom: ")
        try :
            labo[nouveau_nom] = labo[nom]
            modifier_nom(lab, nom, nouveau_nom)
        except AbsentException :
            print(f"{nom} est absent du Laboratoire.")
        except PresentException :
            print(f"{nouveau_nom} est déjà présent dans le laboratoire")


    ajouter_entree_menu(menu, "Modifier le nom d'un membre du personnel", commande_modifier_nom)

    def commande_chercher():
        nom = input("Entrez le nom de la personne que vous cherchez: ")
        try :
            print(f"{nom} est dans le bureau {chercher_membre(lab, nom)}")
        except AbsentException :
            print(f"{nom} est absent du Laboratoire.")

    ajouter_entree_menu(menu, "Chercher un membre du personnel", commande_chercher)

    def commande_trouver_bureau():
        nom = input("Entrez le nom de la personne dont vous cherchez le bureau: ")
        try :
            print(trouver_bureau(lab, nom))
        except AbsentException :
            print(f"{nom} est absent du Laboratoire.")

    ajouter_entree_menu(menu, "Obtenir le bureau d'un membre du personnel", commande_trouver_bureau)

    def commande_imprimer_liste():
        print_txt(lab)

    ajouter_entree_menu(menu, "Imprimer la liste des membres du personnel", commande_imprimer_liste)

    def commande_imprimer_html() :
        print_html(lab)
    
    ajouter_entree_menu(menu, "Imprimer la liste des membres du personnel en format html.",commande_imprimer_html)

    def importer_csv():
        """import d'un fichier csv et utilisation de celui-ci pour créer son laboratoire."""
        fichier = input("rentrer le nom du fichier cvs à ouvrir.")
        try :
            import_csv(fichier, lab)
        except FileNotFoundError :
            print("Le fichier n'existe pas.")

    ajouter_entree_menu(menu, "Importer un laboratoire existant dans un fichier csv.", importer_csv)

    utiliser_menu(menu)

if __name__ == "__main__":
    gestion_labo()
