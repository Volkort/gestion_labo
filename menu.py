"""Module d'affichage du menu de gestion_lab"""


# CLASSES
class WrongInputException(Exception):
    """Exception levée si l'input entré par l'utilisateur
    ne correspond pas à une entrée du menu"""
    pass


# FUNCTIONS
def creation_menu():
    """Initialise une liste pour recevoir les éléments de menu"""
    return []


def ajouter_entree_menu(menu, intitule, commande):
    menu.append([intitule, commande])


def _afficher_menu(menu):
    """Affiche le menu"""
    for num, (intitule, _) in enumerate(menu, 0):
        print("{} - {}".format(num, intitule))


def _input_choix(menu):
    """Permet à l'utilisateur d'entrer le numéro correspondant à son choix"""
    choix = -1
    while choix == -1:    
        try :
            choix = input("Entrez le numéro du menu de votre choix \t")
            if not choix.isdigit() :
                raise ValueError
            choix = int(choix)
            if choix >= len(menu):
                raise WrongInputException
        except ValueError :
            print("Veuillez entrer un entier.")
            choix = -1
        except WrongInputException :
            print("Veuillez entrer le bon index du menu.")
            choix = -1
    return choix


def _traiter_choix(menu, choix):
    """Récupère la commande associé au choix de l'utilisateur"""
    menu[choix][1]()


def utiliser_menu(menu):
    choix = None
    while choix != 0 :
        _afficher_menu(menu)
        choix = _input_choix(menu)
        _traiter_choix(menu, choix)
